
# Python-Lectures  

This is a set of lectures built and extended from sources from Rajeth Kumar, Valentin Haenel, Christian Wallraven, Robert Johansson as well as the official Python tutorial (https://docs.python.org/3.6/tutorial/index.html)

## Introduction

Python is a modern, robust, high level programming language. It is very easy to pick up even if you are completely new to programming.

## Installation

Mac OS X and Linux come pre-installed with python. Windows users can download python from https://www.python.org/downloads/ .

To install IPython run,

    $ pip install ipython jupyter
    
This will install all the necessary dependencies for the notebook, qtconsole, tests etc.

### Installation using packaged environments

Anaconda comes pre-packaged with all the necessary python libraries and also IPython.

#### Anaconda

Download Anaconda from https://www.continuum.io/downloads

Anaconda is completely free and includes more than 300 python packages. Both python 2.7 and 3.6 options are available.


## Launching Jupyter Notebook

In the terminal, go to the directory of the ipython notebooks and type

    jupyter notebook

In Anaconda, open the respective terminals and execute the above.


## Table of contents

### Part I - Python fundamentals

[01 - Numbers, Statements, Built-in types]()

[02 - Control Flow, Functions, File IO]()

[03 - Classes, Modules]()


### Part II - Scientific computing and data analyis

[04 - NumPy introduction part 1]()

[05 - NumPy introduction part 2]()

[06 - Matplotlib introduction]()

[07 - Pandas introduction]()


### Part III - Applications

[TBD]


These are online read-only versions.

## License

This work is licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/

